package com.dummy.products.repository;

import com.dummy.products.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author mgmartinez
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
    
}
