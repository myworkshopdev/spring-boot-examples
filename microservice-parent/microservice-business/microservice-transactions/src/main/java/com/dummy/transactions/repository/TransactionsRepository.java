package com.dummy.transactions.repository;

import com.dummy.transactions.entities.Transactions;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author mgmartinez
 */
public interface TransactionsRepository extends JpaRepository<Transactions, Long> {
    @Query("SELECT t FROM Transactions t WHERE t.accountIban = ?1")
    public List<Transactions> findByAccountIban(String accountIban);
    
    @Query("SELECT t FROM Transactions t WHERE t.reference = ?1")
    public List<Transactions> findByReference(String reference);
}
