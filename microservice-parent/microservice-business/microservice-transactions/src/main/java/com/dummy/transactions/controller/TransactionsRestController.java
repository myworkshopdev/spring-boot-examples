/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dummy.transactions.controller;

import com.dummy.transactions.entities.Transactions;
import com.dummy.transactions.repository.TransactionsRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author mgmartinez
 */
@RestController
@RequestMapping("/transaction")
public class TransactionsRestController {

    @Autowired
    private TransactionsRepository transactionRepo;

    @GetMapping("listTransactions")
    public List<Transactions> getListTransactions() {
        return transactionRepo.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<Transactions> getTransactionById(@PathVariable long id) {
        return transactionRepo.findById(id).map(x -> ResponseEntity.ok(x)).orElse(ResponseEntity.notFound().build());
    }
    
    @GetMapping("{accountIban}")
    public List<Transactions> getTransactionByIban(@RequestParam String accountIban) {
        return transactionRepo.findByAccountIban(accountIban);
    }

    @GetMapping("{reference}")
    public List<Transactions> getTransactionByReference(@RequestParam String reference) {
        return transactionRepo.findByReference(reference);
    }

    @PostMapping
    public ResponseEntity<?> setTransaction(@RequestBody Transactions transaction) {
        Transactions result = new Transactions();
        
        if (transaction.getAmount() != 0) {
            if (transaction.getFee() > 0) {
                Double amount = Math.abs(transaction.getAmount()) - transaction.getFee();
                transaction.setAmount(amount);
            }

            if (transaction.getDate().after(new Date())) {
                transaction.setStatus("01:Pendiente");
            } else if (transaction.getDate().before(new Date()) || transaction.getDate().equals(new Date())) {
                if (transaction.getStatus() == null || transaction.getStatus().isEmpty()) {
                    transaction.setStatus("02:Liquidada");
                }
            }
            
            result = transactionRepo.save(transaction);
            return ResponseEntity.accepted().body(result);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @PutMapping("{id}")
    public ResponseEntity<?> updateTransaction(@PathVariable long id, @RequestBody Transactions transaction) {
        Transactions update = transactionRepo.findById(id).get();
        
        update.setAccountIban(transaction.getAccountIban());
        update.setAmount(transaction.getAmount());
        update.setChannel(transaction.getChannel());
        update.setDate(transaction.getDate());
        update.setDescription(transaction.getDescription());
        update.setFee(transaction.getFee());
        update.setReference(transaction.getReference());
        update.setStatus(transaction.getStatus());
        
        return ResponseEntity.accepted().body(transactionRepo.save(update));
    }
}
